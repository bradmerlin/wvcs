"""
wvcs URL Configuration
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.conf.urls import include, url

from core import views
from frontend.views import app

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^upload', views.commit_form),
    url(r'^files', views.files_list),
    url(r'^repo', views.repo_state),
    url(r'^app', app),
    url('^api/', include('api.urls')),
    url(r'^$', views.commits_list),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
