# -*- coding: utf-8 -*-
"""
URL config for the lmapi module.
"""
from django.conf.urls import include, url
from rest_framework import routers

from . import viewsets

router = routers.DefaultRouter()  # pylint: disable=invalid-name
router.register(r'commits', viewsets.CommitViewSet)
router.register(r'files', viewsets.FilesViewSet)

urlpatterns = [  # pylint: disable=invalid-name
    url(r'^', include(router.urls)),
]
