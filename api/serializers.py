from rest_framework import serializers

from core import models


class CommitSerializer(serializers.ModelSerializer):
    """
    Only really used to allow DRF to save commits. Not really useful for displaying them.
    """
    files = serializers.ListField(child=serializers.FileField(), write_only=True)

    class Meta(object):
        model = models.Commit
        fields = ('files', 'message')

    def create(self, validated_data):
        """
        Use our overloaded `create` method.
        """
        return models.Commit.objects.create(**validated_data)


class FileSerializer(serializers.ModelSerializer):
    """
    List files for display in the UI.
    """
    size = serializers.IntegerField(source='file_pointer.size')
    commit = serializers.SerializerMethodField()

    class Meta(object):
        model = models.File
        fields = (
            'id',
            'commit',
            'create_stamp',
            'file_hash',
            'file_pointer',
            'name',
            'size',)
        extra_kwargs = {
            'file_hash': {'read_only': True}
        }

    def get_commit(self, instance):
        commit = instance.commit
        return {
            'id': commit.id,
            'message': commit.message
        }
