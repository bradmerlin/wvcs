"""
API ViewSets for the WVCS JS frontend.
"""

from rest_framework import viewsets

from core import models
from . import serializers


class CommitViewSet(viewsets.ModelViewSet):
    """
    Commits for this project.
    """
    queryset = models.Commit.objects.all()
    serializer_class = serializers.CommitSerializer

    class Meta:
        model = models.Commit


class FilesViewSet(viewsets.ModelViewSet):
    """
    Files for this project.
    """
    queryset = models.File.objects.all()
    serializer_class = serializers.FileSerializer

    class Meta:
        model = models.Commit
