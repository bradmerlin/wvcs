"""
Tools for WVCS.
"""
import hashlib


def file_hash(file_contents: str) -> str:
    """
    Return a SHA1 hash of the contents of a file.
    Ensures that equivalent hashes mean an equivalent file (except for SHA1 collisions!).
    """
    data_to_hash = str(file_contents).encode('utf-8')
    return hashlib.sha1(data_to_hash).hexdigest()
