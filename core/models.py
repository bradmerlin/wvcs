"""
Model definitions for WVCS.
"""
from django.db import models, transaction

from core import models as core_models
from core import tools


class WVCSModelBase(models.Model):
    """
    Abstract base model for all WVCS models.
    """
    create_stamp = models.DateTimeField(auto_now_add=True)
    modify_stamp = models.DateTimeField(auto_now=True, help_text='This updates whenever modifications are saved.')

    class Meta(object):
        abstract = True


class CommitQuerySet(models.QuerySet):
    """
    Overload create to ensure it always happens the right way.
    """

    def create(self, message: str, files: list):
        # We don't want any files to make it through unless they all do
        with transaction.atomic():
            # Now create commit to mark the project file change.
            commit = Commit(message=message)
            commit.save()

            # Create Files for each file.
            # Still a bit muddly. Reconsider this relationship.
            for file in files:
                core_models.File.objects.create(
                    commit=commit,
                    file_pointer=file,
                    file_hash=tools.file_hash(file.read()),
                    name=file.name)
        return commit


class Commit(WVCSModelBase):
    """
    Represents a change in project files.
    """
    message = models.TextField(help_text='A useful description of the commit.')
    objects = CommitQuerySet.as_manager()

    def __str__(self):
        return self.message


class File(WVCSModelBase):
    """
    Contains metadata for an individual file, and the file object itself. 
    """
    file_pointer = models.FileField(upload_to='uploads/%Y/%m/%d/', help_text='An uploaded file.')
    file_hash = models.TextField(help_text="The SHA1 hash of this file's contents.", max_length=40)
    name = models.TextField(help_text='The given filename. Will be used to attempt to track file versions.')
    commit = models.ForeignKey(Commit, help_text='Which commits this file appears in.', related_name='files')

    def __str__(self):
        return '{}: {}'.format(self.name, self.file_hash)
