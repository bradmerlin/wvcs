"""
Simple views to handle file uploads and displaying data.
"""

from django.db.models import Max
from django.shortcuts import redirect, render

from core import models


def commits_list(request):
    """
    Lists commits in time order ascending
    """
    return render(request, 'core/commits_list.html', context={
        'commits': models.Commit.objects.order_by('-create_stamp'),
    })


def files_list(request):
    """
    Lists files.
    """
    return render(request, 'core/files_list.html', context={
        'files_by_name': models.File.objects.order_by('name'),
        'files_by_hash': models.File.objects.order_by('hash', 'create_stamp'),
    })


def repo_state(request):
    """
    Shows the current state of the repo / branch / whatever we're calling it.
    Massive hack since SQLite doesn't allow `DISTINCT ON`.
    """
    # Group Files by name, and annotate the highest ID to each group.
    # Then get a list of the IDs
    latest_file_ids = models.File.objects.values('name').annotate(
        id__max=Max('id')).values_list('id__max', flat=True)

    # Hit the DB again and get these files.
    current_files = models.File.objects.filter(pk__in=latest_file_ids).order_by('name')
    return render(
        request, 'core/repo.html', context={'files': current_files})


def commit_form(request):
    """
    Simple view function to handle file uploads.
    """
    if request.method == 'POST':
        models.Commit.objects.create(message=request.POST['message'], files=request.FILES.getlist('files'))
        return redirect('/repo')

    # Show template
    return render(request, 'core/commit_form.html')
