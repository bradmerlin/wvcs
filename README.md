## WVCS

#### Wevolver Version Control System

An extremely simple way to keep track of a collection of files.

#### Install

1. `git clone git@bitbucket.org:bradmerlin/wvcs.git && cd wvcs`
2. `pip install -r requirements.txt`
3. `./manage.py migrate`
4. `./manage.py runserver`
5. Visit [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

#### Features

* Only dependencies are Python 3, Django, and SQLite, with no configuration
* Much like a filesystem, uses filename (i.e. path) to keep track of different file versions, history, etc.
* Uses `SHA1(file_contents)` to hash contents for easy file comparisons
* Tracks metadata in an SQLite DB

#### Future features

These can be added easily with the current design

* Add a file pointer model on top of `core.File` to save space by only saving _different_ file contents
* Using file content hash, ask user if they have renamed a file (vs adding a new one with identical file contents)

#### Shortcomings

* Stores a new file every time it is uploaded
* Cannot handle multiple branches, repos, users, etc.
* HEAD is a function of a file's `create_stamp`

#### Timeline

* Started thinking about roughly the same problem since I first created a Wevolver account
* `2017-04-27 11:00` Receive spec from Bram
* `2017-04-27 11:30` Ask a few questions about implementation.
* `2017-04-27 14:00` Start thinking about the problem. Do some reading about how Git works.
* `2017-04-27 15:00` Sketch some models in my new notebook. Run through specs on paper.
* `2017-04-27 15:30` Start coding. Set up boilerplate Django stuff. Write tools for content + commit hashing.
* `2017-04-27 18:00` Finish coding, having mucked about with templates for far too long.
* `2017-04-27 19:00` During dinner thought about some bugs. Fixed them. 
* `2017-04-28 09:00` Confirmed some assumptions with Bram
* `2017-04-28 10:00` Added a nice repo view, `requirements.txt`, `README.md`, removed unnecessary code

### Update

* Added a quick and very dirty React frontend at [http://127.0.0.1:8000/app/](http://127.0.0.1:8000/app/)
* Used many libraries and didn't polish at all. See? I'm actually very versatile.
* Demonstrates understanding of build tools, DRF, bloody class-based views, React and friends including Redux and React Router, ES6 concepts, and incredible coding speed.
* Broke the Django template views.
* Included built assets so you don't need to install build tools.
