"""
One-hit wonder.
"""
from django.shortcuts import render


def app(request):
    """
    The root view for the WVCS frontend.
    """
    return render(request, 'build/index.html')
