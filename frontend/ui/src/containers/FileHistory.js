import React from 'react';
import { connect } from 'react-redux';

import { fetchFiles } from '../actions';

class FileHistory extends React.Component {

    render() {

        const { files } = this.props;

        // Sort by date first, then group by name
        let groupedFiles = Object.values(files)
            .sort((a, b) => ( new Date(a.create_stamp) - new Date(b.create_stamp)))
            .reduce((acc, file) => {
                if (!acc[file['name']]) {
                    acc[file['name']] = []
                }
                acc[file['name']].push(file);
                return acc;
            }, {});
        return (
            <div>
                <h2>FILES</h2>
                {
                    Object.keys(groupedFiles).map(name => {
                        let files_by_name = groupedFiles[name];
                        let first_file = files_by_name[0];
                        return (
                            <div>
                                <h5>{first_file.name}</h5>
                                {
                                    files_by_name.map(file => (
                                        <div>
                                            <p>Size: {file.size} B</p>
                                            <p>Commit ID: {file.commit.id}</p>
                                            <p>Commit message: {file.commit.message}</p>
                                            <p>File hash: <code>{file.file_hash}</code></p>
                                            <hr/>
                                        </div>
                                    ))
                                }
                            </div>
                        )
                    })
                }
            </div>
        );
    }

    componentWillMount = () => {
        // Ensure that we always fetch a new file list.
        // Quick way to avoid having to write to state.
        const { dispatch } = this.props;
        dispatch(fetchFiles());
    }
}

const mapStateToProps = (state) => {
    return {
        files: state.files,
    }
};

export default connect(
    mapStateToProps,
)(FileHistory);
