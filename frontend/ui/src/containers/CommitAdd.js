import React from 'react';
import { browserHistory } from 'react-router';

import Endpoints from '../network/Endpoints';
import FormPost from '../network/FormPost';

class CommitAdd extends React.Component {
    state = {
        message: '',
        files: []
    };

    render() {
        return <div>
            <h4>Add new Commit</h4>
            <input
                type="text"
                placeholder="Start typing..."
                value={this.state.message}
                required
                onChange={this.updateMessage}
            />
            <input type="file" name="files" multiple={true} required="" id="id_files" onChange={this.onDrop}/>
            <input type="submit" value="Submit" onClick={this.postCommit}/>
        </div>
    }

    /**
     * Add the file to state for easy handling later.
     */
    onDrop = (e) => {
        this.setState({ files: e.target.files })
    };

    /**
     * Post FormData to the Commit endpoint
     * @return {*}
     */
    postCommit = () => {
        let data = new FormData();

        data.append('message', this.state.message);
        for (let g = 0; g < this.state.files.length; g++) {
            data.append('files', this.state.files[g]);
        }

        return FormPost(Endpoints.commits, data).then(() => browserHistory.push('/app/file-history'));

    };

    updateMessage = (event) => this.setState({ message: event.target.value });
}

export default CommitAdd;
