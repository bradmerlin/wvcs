import Endpoints from './network/Endpoints';
import Get from './network/Get';

/*
 Action types
 */
export const RECEIVE_FILES = 'RECEIVE_FILES';


/*
 Action creators
 */

/**
 * Signals to process call list
 */
export const receiveFiles = (files) => {
    return { type: RECEIVE_FILES, files };
};


/*
 Thunks
 */

export const fetchFiles = (url = null) => function (dispatch) {
    const request = url ? Get(url) : Get(Endpoints.files);
    return request.then(json => {
        dispatch(receiveFiles(json));
    });
};
