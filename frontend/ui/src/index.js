import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { Provider } from 'react-redux';
import { browserHistory, Route, Router, IndexRedirect } from 'react-router';
import createLogger from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import CommitAdd from './containers/CommitAdd';
import FileHistory from './containers/FileHistory';
import rootAppReducer from './reducers';
import thunkMiddleware from 'redux-thunk';

let middleware = [thunkMiddleware];


export const store = createStore(
    rootAppReducer,
    applyMiddleware(...middleware)
);
if (process.env.NODE_ENV !== 'production') {
    middleware = [...middleware, createLogger()]
}

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/app" component={App}>
                <IndexRedirect to="file-history/"/>
                <Route path="add-commit" component={CommitAdd}/>
                <Route path="file-history" component={FileHistory}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('root')
);
