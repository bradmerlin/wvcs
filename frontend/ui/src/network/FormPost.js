/**
 * Slightly complex POST implementation.
 * Returns a cancellable, chainable Promise.
 * Uses vanilla JS XMLHttpRequest.
 * @param url               Endpoint to POST data to.
 * @param data              Object to be sent. Needs to be multipart FormData instance.
 * @return Promise
 */
const FormPost = (url, data) => {
    let xhr = new XMLHttpRequest();
    let promise = new Promise(function (resolve, reject) {
        xhr.open('POST', url);

        // Resolve or reject accordingly
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(xhr.response);
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        xhr.send(data);

    });

    promise.abort = function () {
        xhr.abort();
    };
    return promise;
};

export default FormPost;
