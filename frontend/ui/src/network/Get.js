/**
 * Default config for GET requests.
 */

const GET_CONFIG = {
    method: 'GET',
    credentials: 'same-origin',
};

/**
 * Simple POST implementation.
 * @param url       Endpoint to POST data to.
 * @param params    Optional query params.
 * @param config    Object to overwrite default configuration.
 * @return Fetch promise
 */
const Get = (url, params = {}, config = {}) => {
    let updatedConfig = Object.assign({}, GET_CONFIG, config);
    const esc = encodeURIComponent;
    const query = Object.keys(params)
        .map(k => esc(k) + '=' + esc(params[k]))
        .join('&');

    return fetch(`${url}?${query}`, updatedConfig)
        .then(response => {
            return response.json();
        });
};

export default Get;
