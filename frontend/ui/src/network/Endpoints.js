const root = 'api';
export default {
    commits: `/${root}/commits/`,
    files: `/${root}/files/`,
};
