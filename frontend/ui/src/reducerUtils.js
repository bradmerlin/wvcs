export const loadListToState = (state, action, key, getter) => {
    return action[key].reduce((ret, current) => {
        ret[current[getter]] = current;
        return ret;
    }, { ...state });
};
