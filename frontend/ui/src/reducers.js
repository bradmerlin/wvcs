import { combineReducers } from 'redux';

import * as actions from './actions';
import { loadListToState } from './reducerUtils';


const fileReducer = (state = {}, action) => {
    switch (action.type) {
        case actions.RECEIVE_FILES:
            return loadListToState(state, action, 'files', 'id');
        default:
            return state;
    }
};


const rootAppReducer = combineReducers({
    files: fileReducer,
});

export default rootAppReducer;
